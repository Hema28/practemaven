package com.selenium.practemaven;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class AssignementTwo {
	
	WebDriver driver;
	
  @BeforeClass
  public void beforeClass() {
	  System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
	   driver  =  new ChromeDriver();
	  
  }

  @Test
  public void facebook() {
	  driver.get("https://facebook.com");
	  String url=driver.getCurrentUrl();
	  System.out.println("Current url"+url);
	  Assert.assertEquals(url, "https://www.facebook.com/");
  }
  
  @Test
  public void loginDetails() {
	WebElement fname=driver.findElement(By.xpath("//input[@id='u_0_c']"));  
	fname.sendKeys("sherlie");
	WebElement lname=driver.findElement(By.xpath("//input[@id='u_0_e']"));
	lname.sendKeys("reddy");
	WebElement email=driver.findElement(By.xpath("//input[@aria-describedby='js_2s']"));
	email.sendKeys("sherliereddy440@gmail.com");
	WebElement npwd=driver.findElement(By.xpath("//input[@id='u_0_11']"));
	npwd.sendKeys("Rushikasamanvi");
	
  }
  @AfterClass
  public void afterClass() {
  }

}
