package com.selenium.practemaven;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;

public class TestingFb {
	
	ChromeDriver driver;

	@BeforeClass
	public void beforeClass() {
	

	}
	
 @BeforeMethod
  public void beforeMethod() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver=new ChromeDriver(); 
	driver.get("https://www.facebook.com");
  }

 @DataProvider(name="fb") 
	public Object[][] dataProvider(){
		
		Object[][] inp= {{"id1","pwd1"},{"testingd360@gmail.com","Rushisamanvi"},{"id3","pwd3"}};
		return inp;
		
	}
 
 @Test(dataProvider="fb")
 public void fbLogin(String id,String pwd) {
	 WebElement email=driver.findElement(By.xpath("//input[@type='email']"));
	 email.sendKeys(id);
	 WebElement password=driver.findElement(By.xpath("//input[@tabindex='2']"));
	 password.sendKeys(pwd);
	 WebElement login=driver.findElement(By.xpath("//input[@value='Log In']"));
	 login.click();
      //List<WebElement>  isPresent =driver.findElements(By.xpath("//span[@class='_50f6']"));
      
      Boolean  isPresent1 =driver.findElements(By.xpath("//span[@class='_50f6']")).size()>0;
      if (isPresent1 == true)
      {
    	  System.out.println("login failed");
      }
      else {
    	  System.out.println("Login success");
      }
      }
    	  
   
  @AfterMethod
  public void afterMethod() {
	   driver.quit();
  }
  

  @AfterClass
  public void afterClass() {
  }

}
