package com.selenium.practemaven;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class AssignmentOne {
	WebDriver driver;
	WebDriverWait wait;
  
  @BeforeClass
  public void beforeClass() {
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
	    driver = new ChromeDriver();
	    driver.manage().window().maximize();
	    driver.get("http://qatechhub.com");
  }
  @BeforeMethod
  public void beforeMethod() {
		 wait = new WebDriverWait(driver,10);
  }
  
  @Test(priority=10,description="quatehub")
  public void qateHub() {
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@data-css='tve-u-205bf0164739eeb']"))).click();
	  String title=driver.getTitle();
	  System.out.println("titile"+title);
	  Assert.assertEquals(title, "QA utomation Tools Trainings and Tutorials | QA Tech Hub");
  }

  @Test(priority=20,description="facebook")
  public void faceBook() {
	 // driver.get("https://www.facebook.com");
	  driver.navigate().to("https://www.facebook.com");
	  
	  driver.navigate().back();
	  String url=driver.getCurrentUrl();
	  System.out.println("Current page " + url);
	  driver.navigate().forward();
	  driver.navigate().refresh();
	  driver.close();
  }

  @AfterClass
  public void afterClass() {
  }

}
