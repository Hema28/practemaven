package com.selenium.practemaven;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;

public class HardCode {
  
	WebDriver driver;
	
  @BeforeClass
  @Parameters(value="data")
  public void beforeClass(String x )throws IOException {
	  File ip =new File("./ip.properties");
	  InputStream data=new FileInputStream(ip);
	  Properties pro=new Properties();
	  pro.load(data);
	  String url=pro.getProperty(x);
	  System.out.println(url);
	  
	  System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	  driver = new ChromeDriver(); 
	  driver.get(url);
  }
  
  
  @BeforeMethod
  
  public void beforeMethod()  {

  }
  @Test
  public void f() {
  }
  
  @AfterClass
  public void afterClass() {
  }
  
  @AfterMethod
  public void afterMethod() {
  }
}
