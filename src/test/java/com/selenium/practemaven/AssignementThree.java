package com.selenium.practemaven;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterClass;

public class AssignementThree {
  
  WebDriver driver;

  @BeforeClass
   @Parameters(value="brow")
   public void beforeClass(String  browser ) {
      System.out.println(browser);
	  if (browser.equals("chrome")) {
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		  }
	  else if(browser.equals("explorer")) {
		   System.setProperty("webdriver.ie.driver","./Drivers/IEDriverServer.exe");
		   driver = new InternetExplorerDriver();
	  }
	  else {
		  System.out.println("please enter the prper string");
	  }
		  
  }

  @Test
  public void flipkart() {
	  driver.get("https://www.flipkart.com/");
	  driver.manage().window().maximize();
	  WebElement pop=driver.findElement(By.xpath("//button[@class='_2AkmmA _29YdH8']"));
	  pop.click();
	  int count =0;
	  for (WebElement link:driver.findElements(By.tagName("a"))) {
		  System.out.println(link.getText());
		  count++;
	  }
		  
  }

  @AfterClass
  public void afterClass() {
  }

}
