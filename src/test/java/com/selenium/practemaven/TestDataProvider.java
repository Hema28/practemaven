package com.selenium.practemaven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


public class TestDataProvider {
	
	
	WebDriver driver; 
	@DataProvider(name="id_pwd")
	public Object[][] dataProvider() {
		
		Object[][] obj = {{"id1","pwd1"},{"id2","pwd2"},{"id3","pwd3"}};
		
		return obj;
	}
	
	
	@BeforeMethod
	@Parameters(value="name")
	public void beforeClass(String x) throws IOException {
		
		
		
		File file =  new File("./"+x+".properties");// recognise it as a File
		
		InputStream is = new FileInputStream(file);// inputing the data from the same file
		
		Properties prop = new Properties();
		prop.load(is);
		String url = prop.getProperty("fb");
		System.out.println("The url is "+url);
		
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(url);
		
	}
	
	//@Test(dataProvider="id_pwd")
	public void tes1(String id, String password) {
		System.out.println("The id is "+id);
		System.out.println("The password is "+password);
		
	}
	
	@Test	
	public void paraTest() {		
		
	}
	
	@AfterMethod
	public void afterClass() {
		
		//driver.quit();
	}

}
