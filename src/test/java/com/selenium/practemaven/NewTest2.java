package com.selenium.practemaven;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;

public class NewTest2 {
	WebDriver driver;

  @BeforeClass
  @Parameters(value="browser")
  public void beforeClass(String x) {
//	  public void beforeClass() {
	  System.out.println(x);
	  if(x.equals("firefox")) {
		  System.out.println(x);
	  System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
	  driver = new ChromeDriver();
	  driver.get("http://www.fb.com");
	  }
	  
	  else if(x.equals("explorer")) {
	  System.setProperty("webdriver.ie.driver", "./Drivers/IEDriverServer.exe");
	  driver= new InternetExplorerDriver(); 
	  driver.get("http://www.google.ca");
	  	  }
	  else {
		  System.out.println("please pass firefox or explorer");
	  }
  }
  
  
  @Test
  public void f() {
	  String title = driver.getTitle();
	  System.out.println("The title is"+title);
  }

  @AfterClass
  public void afterClass() {
	  System.out.println("This is after class Class2");
  }

}
