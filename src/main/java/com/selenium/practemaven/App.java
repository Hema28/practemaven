package com.selenium.practemaven;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Hello world!
 *
 */
public class App 
{
	
	static WebDriver driver;
    public static void main( String[] args ) throws InterruptedException
    {
    	System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
        driver = new ChromeDriver();
        
        driver.get("file:///C:/Users/ijjad/Desktop/aler.html");
        WebElement clickBtn = driver.findElement(By.xpath("//button[text()=' Click Here']"));
        clickBtn.click();
        
        Thread.sleep(3000);
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.alertIsPresent());
              
        Alert alert = driver.switchTo().alert();
        alert.accept();
        
        App.fbDropdown();
        
        
        
    }
    
    public static void fbDropdown() {
    	
    	driver.get("http://www.fb.com");
    	
    	WebElement month = driver.findElement(By.xpath("//select[@aria-label='Month']"));
    	
    	Select select = new Select(month);
    	select.selectByVisibleText("Feb");
    	 	
    	WebElement year = driver.findElement(By.xpath("//select[@aria-label='Year']"));
    	
    	Select selectYear = new Select(year);
    	selectYear.selectByVisibleText("2010");
    }
}
